### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 6c0a7a70-ccac-11ec-2f7f-cbe7098298d9
using DataStructures

# ╔═╡ fcad6139-b3f4-41c4-9ae4-099e6be4252c
using Test

# ╔═╡ fbbeb1f7-924e-44e1-9e1c-9ddc8c25f1a8
using AStarSearch

# ╔═╡ e8323177-51f1-48d8-b93c-18fa10827870
const EAST = CartesianIndex(0, 1)

# ╔═╡ 732187c5-85c8-4b1c-903e-7898cd915afe
const WEST = CartesianIndex(0, -1)

# ╔═╡ d6e1a337-2034-44ea-a3b3-d745865eb331
const UP = CartesianIndex(-1, 0)

# ╔═╡ 6a51211e-26c6-4b85-9e39-7b8bba7c00c4
const DOWN = CartesianIndex(1, 0)

# ╔═╡ cfa6ff53-aff8-45f4-a452-64788d80c54d
const COLLECT = CartesianIndex(0, 0)

# ╔═╡ b5987d5c-f749-48ef-aff0-552c6c275c30
const DIRECTIONS = (UP, DOWN, EAST, WEST)

# ╔═╡ cef88ac6-a5c8-4cd9-929c-e81f62c03484
manhattan(a::CartesianIndex, b::CartesianIndex)=sum(abs.((b - a).I))

# ╔═╡ a92704ad-31f1-4811-bb5c-d804105dc1f0
function parcelsneighbours(parcels, p)
	res = CartesianIndex[]
	for d in DIRECTIONS
		n = p + d
		if 1 ≤ n[1] ≤ size(parcels)[1] && 1 ≤ n[2] ≤ size(parcels)[2] && !parcels[n]
			push!(res, n)
		end
	end
	return res
end

# ╔═╡ 708210c6-2f29-4795-bb14-ece48dca1c4d
function solveparcels(parcels, start, goal)
	currentparcelsneighbours(state)=parcelsneighbours(parcels, state)
	return astar(currentparcelsneighbours, start, goal, heuristic=manhattan)
end	

# ╔═╡ f1f6034e-11be-4c5d-8b99-976289d8ba8e
begin
	parcels = [0 0 1 0 0;
			0 1 0 0 0;
		    0 1 0 0 1;
			0 0 0 1 1;
			1 0 1 0 0] .==1
	start = CartesianIndex(1, 1)
	goal = CartesianIndex(1, 5)	
end

# ╔═╡ 8c8d6b04-6f63-4b3c-940e-e784da4e5258
res = solveparcels(parcels, start, goal)

# ╔═╡ 2bf8fbdb-1475-4e53-af68-466421e4ba04
@test res.status == :success

# ╔═╡ 52042ce4-db6d-4a69-930a-25e971e3afdc
@test res.path == CartesianIndex{2}[
	CartesianIndex(1, 1),
	CartesianIndex(2, 1),
	CartesianIndex(3, 1),
	CartesianIndex(4, 1),
	CartesianIndex(4, 2),
	CartesianIndex(4, 3),
	CartesianIndex(3, 3),
	CartesianIndex(2, 3),
	CartesianIndex(2, 4),
	CartesianIndex(1, 4),
	CartesianIndex(1, 5)]

# ╔═╡ c5d6fd99-7a48-41ac-bf6b-d53b97b1328f
@test res.cost == 10

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
AStarSearch = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Test = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[compat]
AStarSearch = "~0.5.3"
DataStructures = "~0.18.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AStarSearch]]
deps = ["DataStructures"]
git-tree-sha1 = "a87235526b086001b4670bb950d76ffe861c8d83"
uuid = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
version = "0.5.3"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═6c0a7a70-ccac-11ec-2f7f-cbe7098298d9
# ╠═fcad6139-b3f4-41c4-9ae4-099e6be4252c
# ╠═fbbeb1f7-924e-44e1-9e1c-9ddc8c25f1a8
# ╠═e8323177-51f1-48d8-b93c-18fa10827870
# ╠═732187c5-85c8-4b1c-903e-7898cd915afe
# ╠═d6e1a337-2034-44ea-a3b3-d745865eb331
# ╠═6a51211e-26c6-4b85-9e39-7b8bba7c00c4
# ╠═cfa6ff53-aff8-45f4-a452-64788d80c54d
# ╠═b5987d5c-f749-48ef-aff0-552c6c275c30
# ╠═cef88ac6-a5c8-4cd9-929c-e81f62c03484
# ╠═a92704ad-31f1-4811-bb5c-d804105dc1f0
# ╠═708210c6-2f29-4795-bb14-ece48dca1c4d
# ╠═f1f6034e-11be-4c5d-8b99-976289d8ba8e
# ╠═8c8d6b04-6f63-4b3c-940e-e784da4e5258
# ╠═2bf8fbdb-1475-4e53-af68-466421e4ba04
# ╠═52042ce4-db6d-4a69-930a-25e971e3afdc
# ╠═c5d6fd99-7a48-41ac-bf6b-d53b97b1328f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
